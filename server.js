
'use strict'
const Hapi = require('hapi');
const Boom = require('boom');
const mongoose = require('mongoose');
const glob = require('glob');
const path = require('path');
const secret = require('./config');
const HapiNowAuth = require('@now-ims/hapi-now-auth');

const dbUrl = 'mongodb://mongo:27017/super-heros-db';
global.db = mongoose.createConnection(dbUrl);

const server = Hapi.server({
  host: '0.0.0.0',
  port: 3000
});

async function start() {

  try {
    await server.register(HapiNowAuth);
  }
  catch (error) {
    console.error(error);
    process.exit(1);
  }

  server.auth.strategy('jwt-strategy', 'hapi-now-auth', {
    verifyJWT: true,
    keychain: [secret],
    verifyOptions: { algorithms: ['HS256'] },
    validate: async (request, token, h) => {
      const credentials = token.decodedJWT;
      const isValid = true;
      return { isValid, credentials };
    }
  });

  server.auth.default('jwt-strategy');

  glob.sync('api/**/routes/*.js', {
    root: __dirname
  }).forEach(file => {
    const route = require(path.join(__dirname, file));
    server.route(route);
  });

  try {
    await server.start();
  }
  catch (error) {
    console.error(error);
    process.exit(1);
  }

  console.log(`Server running at: ${server.info.uri}`);
};

start();

module.exports = server;
