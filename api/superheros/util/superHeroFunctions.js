'use strict';
const Boom = require('boom');
const SuperHero = require('../model/SuperHero');
const SuperPower = require('../../superpowers/model/SuperPower').db;

function verifyUniqueSuperHero(req, h) {
    return new Promise((resolve, reject) => {
        SuperHero.findOne({
            name: req.payload.name
        }, (err, superHero) => {
            if (superHero) {
                if (superHero.name === req.payload.name) {
                    reject(Boom.badRequest('SuperHero name taken'));
                    return
                }
            }
            resolve(req.payload);
        });
    });
};

function findSuperPower(_powers) {
    return new Promise((resolve, reject) => {
        getSuperPowers(_powers)
            .then(powers => { resolve(powers); })
            .catch(err => reject(err))
    });
};

function getSuperPowers(powers) {
    return new Promise((resolve, reject) => {
        let powersId = [];
        powers.forEach(power => {
            powersId.push(power);
        });

        SuperPower.find(
            {
                '_id': { $in: powersId }
            }, (err, _powers) => {
                if (_powers === null || _powers === undefined ||
                    _powers.length === 0 || err) {
                    reject('Super Power not found');
                    return
                }
                resolve(_powers);
            });
    });
};


function containsSuperPower(superPowerId) {
    return new Promise((resolve, reject) => {
        SuperHero.find({}, (err, superHeros) => {
            let superPowerFound = false;
            superHeros.forEach(superHero => {
                superHero.powers.forEach(power => {
                    if (superPowerId === power._id.toString()) {
                        superPowerFound = true;
                    }
                });
            });
            if (superPowerFound) {
                resolve(true);
            } else {
                reject(false);
            }
        });
    });
};

module.exports = {
    verifyUniqueSuperHero: verifyUniqueSuperHero,
    findSuperPower: findSuperPower,
    containsSuperPower: containsSuperPower
};