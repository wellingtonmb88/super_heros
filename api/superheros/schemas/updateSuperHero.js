'use strict';
const Joi = require('joi');
const updateSuperHeroSchema = Joi.object({
  _id: Joi.string().required(),
  name: Joi.string().min(2).max(30).required(),
  alias: Joi.string().required(),
  powers: Joi.array().items(Joi.string())
});
module.exports = updateSuperHeroSchema;