'use strict';
const Joi = require('joi');
const createSuperHeroSchema = Joi.object({
  name: Joi.string().min(2).max(30).required(),
  alias: Joi.string().required(),
  powers: Joi.array().items(Joi.string())
});
module.exports = createSuperHeroSchema;