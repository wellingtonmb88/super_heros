'use strict';
const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const SuperPowerModel = require('../../superpowers/model/SuperPower').superPowerModel;

const superHeroModel = new Schema({
    name: { type: String, required: true, unique: true },
    alias: { type: String, required: true },
    powers: [SuperPowerModel]
});

module.exports = db.model('SuperHero', superHeroModel);