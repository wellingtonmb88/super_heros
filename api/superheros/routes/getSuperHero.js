
'use strict';
const SuperHero = require('../model/SuperHero');
const Boom = require('boom');

function getSuperHero(req) {
  return new Promise((resolve, reject) => {

    SuperHero.findOne({ _id: req.params.id }, '-__v',
      function (err, superHero) {
        if (err) {
          reject(Boom.badRequest(err));
          return
        }
        resolve(superHero);
      });
  });
};

module.exports = {
  method: 'GET',
  path: '/api/superheros/{id}',
  config: {
    handler: async (req, res) => {
      return getSuperHero(req)
        .then(superHero => { return superHero })
        .catch(error => { throw error });
    },
    auth: false
  }
};