'use strict';
const SuperHero = require('../model/SuperHero');
const saveAuditEvent = require('../../auditing/util/auditEventFunctions').saveAuditEvent;

function deleteSuperHero(req) {
  return new Promise((resolve, reject) => {
    SuperHero.findOne({ _id: req.params.id }, function (err, superHero) {
      if (err || superHero === null) {
        reject(Boom.badRequest("SuperHero not found!"));
        return
      }
      saveAuditEvent("SuperHero", superHero._id, req.auth.credentials.username, "delete");
      superHero.remove();
      resolve({ success: true, message: "SuperHero deleted successully!" });
    });
  });
};

module.exports = {
  method: 'DELETE',
  path: '/api/superheros/{id}',
  config: {
    handler: async (req, res) => {
      return deleteSuperHero(req)
        .then(success => { return success })
        .catch(error => { return error });
    },
    auth: {
      strategy: 'jwt-strategy',
      scope: ['admin']
    }
  }
};