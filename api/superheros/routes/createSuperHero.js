'use strict';
const bcrypt = require('bcrypt');
const Boom = require('boom');
const SuperHero = require('../model/SuperHero');
const SuperPower = require('../../superpowers/model/SuperPower').db;
const createSuperHeroSchema = require('../schemas/createSuperHero');
const verifyUniqueSuperHero = require('../util/superHeroFunctions').verifyUniqueSuperHero;
const findSuperPower = require('../util/superHeroFunctions').findSuperPower;
const saveAuditEvent = require('../../auditing/util/auditEventFunctions').saveAuditEvent;


function saveSuperHero(superHero, req) {
  return new Promise((resolve, reject) => {
    superHero.save((err, superHero) => {
      if (err) {
        reject(Boom.badRequest(err));
        return
      }
      if (superHero === undefined) {
        reject(Boom.badRequest("SuperHero undefined!"));
        return
      }
      saveAuditEvent("SuperHero", superHero._id, req.auth.credentials.username, "create");
      resolve(superHero);
    });
  });
};

function tryToSaveSuperHero(req) {
  return new Promise((resolve, reject) => {
    let superHero = new SuperHero();
    superHero.name = req.payload.name;
    superHero.alias = req.payload.alias;
    superHero.powers = [];
    const payloadPowers = req.payload.powers;

    if (payloadPowers && payloadPowers.length > 0) {
      findSuperPower(payloadPowers)
        .then(powers => {
          if (powers === null || payloadPowers.length !== powers.length) {
            reject(Boom.badRequest('Super Power not found'));
            return
          } else {
            superHero.powers = powers;
            saveSuperHero(superHero, req)
              .then(success => resolve(success))
              .catch(err => reject(Boom.badRequest(err)));
          }
        })
        .catch(err => reject(Boom.badRequest(err)))
    } else {
      saveSuperHero(superHero, req)
        .then(success => resolve(success))
        .catch(err => reject(Boom.badRequest(err)));
    }
  });
};

module.exports = {
  method: 'POST',
  path: '/api/superheros',
  config: {
    pre: [
      { method: verifyUniqueSuperHero, assign: 'superhero' }
    ],
    handler: async (req, res) => {
      return tryToSaveSuperHero(req)
        .then(success => { return success })
        .catch(error => { return error })
    },
    validate: {
      payload: createSuperHeroSchema
    },
    auth: {
      strategy: 'jwt-strategy',
      scope: ['admin']
    }
  }
};