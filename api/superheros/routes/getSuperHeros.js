
'use strict';
const SuperHero = require('../model/SuperHero');
const Boom = require('boom');
const Joi = require('joi');

const LIMIT = 100;

function getAllSuperHeros(req) {
  return new Promise((resolve, reject) => {
    const page = req.query.page;
    const offset = (page - 1) * LIMIT;

    SuperHero.find({}, '-__v',
      { skip: offset, limit: LIMIT }, function (err, superHeros) {
        if (err) {
          reject(Boom.badRequest(err));
        }
        resolve({
          page: page,
          count_total: superHeros.length,
          results: superHeros
        });
      });
  });
};

module.exports = {
  method: 'GET',
  path: '/api/superheros',
  config: {
    validate: {
      query: {
        page: Joi.number().positive()
      }
    },
    handler: async (req, res) => {
      return getAllSuperHeros(req)
        .then(superHeros => { return superHeros })
        .catch(error => { throw error });
    },
    auth: false
  }
};