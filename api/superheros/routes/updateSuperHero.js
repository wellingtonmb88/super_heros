'use strict';
const Boom = require('boom');
const SuperHero = require('../model/SuperHero');
const updateSuperHeroSchema = require('../schemas/updateSuperHero');
const verifyUniqueSuperHero = require('../util/superHeroFunctions').verifyUniqueSuperHero;
const findSuperPower = require('../util/superHeroFunctions').findSuperPower;
const saveAuditEvent = require('../../auditing/util/auditEventFunctions').saveAuditEvent;

function updateSuperHero(superHero, req) {
  return new Promise((resolve, reject) => {
    superHero.alias = req.payload.alias;
    superHero.save((err, raw) => {
      if (err) {
        reject(Boom.badRequest(err));
        return
      }
      if (raw === undefined) {
        reject(Boom.badRequest("SuperHero undefined!"));
        return
      }
      saveAuditEvent("SuperHero", raw._id, req.auth.credentials.username, "update");
      resolve(raw);
    });
  });
};

function tryToUpdateSuperHero(req, res) {
  return new Promise((resolve, reject) => {
    SuperHero.findOne({ _id: req.payload._id }, function (err, superHero) {
      if (err || superHero === null) {
        reject(Boom.badRequest("SuperHero undefined!"));
        return
      }

      if (superHero.name !== req.payload.name) {
        verifyUniqueSuperHero(req, res)
          .then(() => {
            superHero.name = req.payload.name;
          }).catch(err => reject(Boom.badRequest(err)))
      }

      const payloadPowers = req.payload.powers;
      if (payloadPowers && payloadPowers.length > 0) {
        findSuperPower(payloadPowers)
          .then(powers => {
            if (powers === null || payloadPowers.length !== powers.length) {
              reject(Boom.badRequest('Super Power not found'));
              return
            } else {
              superHero.powers = powers;
              updateSuperHero(superHero, req)
                .then(success => resolve(success))
                .catch(err => reject(Boom.badRequest(err)));
            }
          })
          .catch(err => reject(Boom.badRequest(err)))
      } else {
        superHero.powers = [];
        updateSuperHero(superHero, req)
          .then(success => resolve(success))
          .catch(err => reject(Boom.badRequest(err)));
      }
    });
  });
};

module.exports = {
  method: 'PUT',
  path: '/api/superheros',
  config: {
    handler: async (req, res) => {
      return tryToUpdateSuperHero(req, res)
        .then(success => { return success })
        .catch(error => { return error })
    },
    validate: {
      payload: updateSuperHeroSchema
    },
    auth: {
      strategy: 'jwt-strategy',
      scope: ['admin']
    }
  }
};