'use strict';
const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const auditEventModel = new Schema({
    entity: { type: String, required: true },
    entityId: { type: String, required: true },
    datetime: { type: Number, required: true },
    username: { type: String, required: true },
    action: { type: String, required: true }
});

module.exports = db.model('AuditEvent', auditEventModel);