'use strict';
const Joi = require('joi');
const auditEventSchema = Joi.object({
  entity: Joi.string().required(),
  entityId: Joi.string().required(),
  datetime: Joi.number().required(),
  username: Joi.string().alphanum().min(2).max(30).required(),
  action: Joi.string().required(),
});
module.exports = auditEventSchema;