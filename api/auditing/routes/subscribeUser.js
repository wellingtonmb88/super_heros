'use strict';
const Boom = require('boom');

module.exports = {
  method: 'POST',
  path: '/api/auditing/subscribe',
  config: {
    handler: async (req, h) => {
        return Boom.badRequest("API undefined!");
    },
    auth: false
  }
}