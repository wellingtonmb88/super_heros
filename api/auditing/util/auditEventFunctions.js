'use strict';

const AuditEvent = require('../model/AuditEvent');

function saveAuditEvent(entity, entityId, username, action) {
  let auditEvent = new AuditEvent();
  auditEvent.entity = entity;
  auditEvent.entityId = entityId;
  auditEvent.username = username;
  auditEvent.datetime = new Date().getTime();
  auditEvent.action = action.toUpperCase();

  auditEvent.save((err, auditEvent) => {
    if (err) {
      console.error("Error to save AuditEvent: " + err);
      return
    }
    console.log("AuditEvent saved!");
    //the event is registered it has to broadcast
    //to every user that subscribed to the event using endpoint 16.
  });
}

function subscribe() {

}

module.exports = {
  saveAuditEvent: saveAuditEvent,
  subscribe: subscribe,
};