'use strict';
const Joi = require('joi');
const createUserSchema = Joi.object({
  username: Joi.string().alphanum().min(2).max(30).required(),
  password: Joi.string().required(),
  roles: Joi.array().items(Joi.object({ name: Joi.string().required() })).required()
});
module.exports = createUserSchema;