'use strict';
const Boom = require('boom');
const User = require('../model/User');
const authenticateUserSchema = require('../schemas/authenticateUser');
const verifyCredentials = require('../util/userFunctions').verifyCredentials;
const createToken = require('../util/token');

module.exports = {
  method: 'POST',
  path: '/api/users/authenticate',
  config: {
    pre: [
      { method: verifyCredentials, assign: 'user' }
    ],
    handler: async (req, h) => {
      const user = req.pre.user;
      if (!user) {
        return Boom.badRequest("User undefined!");
      }
      return { id_token: createToken(req.pre.user) };
    },
    validate: {
      payload: authenticateUserSchema
    },
    auth: false
  }
}