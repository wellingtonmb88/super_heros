
'use strict';
const User = require('../model/User');
const Boom = require('boom');
const Joi = require('joi');

const LIMIT = 100;

function getAllUsers(req) {
  return new Promise((resolve, reject) => {
    const page = req.query.page;
    const offset = (page - 1) * LIMIT;

    // Deselect the password and version fields
    User.find({}, '-password -__v',
      { skip: offset, limit: LIMIT }, function (err, users) {
        if (err) {
          reject(Boom.badRequest(err));
        }
        resolve({
          page: page,
          count_total: users.length,
          results: users
        });
      });
  });
};

module.exports = {
  method: 'GET',
  path: '/api/users',
  config: {
    validate: {
      query: {
        page: Joi.number().positive()
      }
    },
    handler: async (req, res) => {
      return getAllUsers(req).then(users => {
        return users;
      }).catch(error => { throw error })
    },
    auth: {
      strategy: 'jwt-strategy',
      scope: ['admin']
    }
  }
};