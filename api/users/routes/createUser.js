'use strict';
const bcrypt = require('bcrypt');
const Boom = require('boom');
const User = require('../model/User');
const Role = require('../model/Role');
const createUserSchema = require('../schemas/createUser');
const verifyUniqueUser = require('../util/userFunctions').verifyUniqueUser;
const findRole = require('../util/userFunctions').findRole;
const hashPassword = require('../util/userFunctions').hashPassword;
const createToken = require('../util/token');
const saveAuditEvent = require('../../auditing/util/auditEventFunctions').saveAuditEvent;

function saveUser(req) {
  return new Promise((resolve, reject) => {
    let user = new User();
    const payloadRoles = req.payload.roles;
    user.username = req.payload.username;

    findRole(payloadRoles).then(roles => {
      if (roles === null || payloadRoles.length !== roles.length) {
        reject(Boom.badRequest('Role not found'));
        return
      } else {
        user.roles = roles;
      }

      hashPassword(req.payload.password, (err, hash) => {
        if (err) {
          reject(Boom.badRequest(err));
          return
        }
        user.password = hash;
        user.save((err, _user) => {
          if (err) {
            reject(Boom.badRequest(err));
            return
          }
          if (user === undefined) {
            reject(Boom.badRequest("User undefined!"));
            return
          }
          saveAuditEvent("USER", user._id, req.auth.credentials.username, "create");
          resolve({ id_token: createToken(user) });
        });
      });
    }).catch(err => reject(Boom.badRequest(err)));
  });
}

module.exports = {
  method: 'POST',
  path: '/api/users',
  config: {
    pre: [
      { method: verifyUniqueUser, assign: 'user' }
    ],
    handler: async (req, res) => {
      return saveUser(req)
        .then(success => { return success })
        .catch(error => { return error })
    },
    validate: {
      payload: createUserSchema
    },
    auth: {
      strategy: 'jwt-strategy',
      scope: ['admin']
    }
  }
}