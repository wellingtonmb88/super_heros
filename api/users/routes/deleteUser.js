'use strict';
const bcrypt = require('bcrypt');
const Boom = require('boom');
const User = require('../model/User');
const Role = require('../model/Role');
const createToken = require('../util/token');
const saveAuditEvent = require('../../auditing/util/auditEventFunctions').saveAuditEvent;

function deleteUser(req) {
  return new Promise((resolve, reject) => {
    User.findOne({ _id: req.params.id }, function (err, user) {
      if (err || user === null) {
        reject(Boom.badRequest("User not found!"));
        return
      }
      saveAuditEvent("USER", user._id, req.auth.credentials.username, "delete");
      user.remove();
      resolve({ success: true, message: "User deleted successully!" });
    });
  });
};

module.exports = {
  method: 'DELETE',
  path: '/api/users/{id}',
  config: {
    handler: async (req, res) => {
      return deleteUser(req)
        .then(success => { return success })
        .catch(error => { return error });
    },
    auth: {
      strategy: 'jwt-strategy',
      scope: ['admin']
    }
  }
}