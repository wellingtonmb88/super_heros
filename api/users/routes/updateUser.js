'use strict';
const bcrypt = require('bcrypt');
const Boom = require('boom');
const User = require('../model/User');
const Role = require('../model/Role');
const updateUserSchema = require('../schemas/updateUser');
const verifyUniqueUser = require('../util/userFunctions').verifyUniqueUser;
const findRole = require('../util/userFunctions').findRole;
const hashPassword = require('../util/userFunctions').hashPassword;
const createToken = require('../util/token');
const saveAuditEvent = require('../../auditing/util/auditEventFunctions').saveAuditEvent;


function updateUser(req, res) {
  return new Promise((resolve, reject) => {
    const payloadRoles = req.payload.roles;

    findRole(payloadRoles).then(roles => {
      if (roles === null || payloadRoles.length !== roles.length) {
        reject(Boom.badRequest('Role not found'));
        return
      }

      User.findOne({ _id: req.payload._id }, function (err, user) {
        if (err || user === null) {
          reject(Boom.badRequest("User undefined!"));
          return
        }

        if (user.username !== req.payload.username) {
          verifyUniqueUser(req, res)
            .then(() => { user.username = req.payload.username })
            .catch(err => reject(Boom.badRequest(err)))
        }

        user.roles = roles;
        hashPassword(req.payload.password, (err, hash) => {
          if (err) {
            reject(Boom.badRequest(err));
            return
          }
          user.password = hash;
          user.save((err, _user) => {
            if (err) {
              reject(Boom.badRequest(err));
              return
            }
            if (user === undefined) {
              reject(Boom.badRequest("User undefined!"));
              return
            }

            saveAuditEvent("USER", user._id, req.auth.credentials.username, "update");
            resolve({ id_token: createToken(user) });
          });
        });
      });
    }).catch(err => reject(Boom.badRequest(err)));
  });
};

module.exports = {
  method: 'PUT',
  path: '/api/users',
  config: {
    handler: async (req, res) => {
      return updateUser(req, res)
        .then(success => { return success })
        .catch(error => { return error });
    },
    validate: {
      payload: updateUserSchema
    },
    auth: {
      strategy: 'jwt-strategy',
      scope: ['admin']
    }
  }
}