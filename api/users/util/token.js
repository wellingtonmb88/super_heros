'use strict';
const jwt = require('jsonwebtoken');
const secret = require('../../../config');

function containsRole(roles, obj) {
  var i = roles.length;
  while (i--) {
    if (roles[i] !== null &&
        roles[i].name === obj) {
      return true;
    }
  }
  return false;
}

function createToken(user) {
  let scopes;
  // Check if the user object passed in
  // has admin set to true, and if so, set
  // scopes to admin
  if (containsRole(user.roles, 'admin')) {
    scopes = 'admin';
  }
  // Sign the JWT
  return jwt.sign(
    {
      id: user._id,
      username: user.username,
      scope: scopes
    },
    secret,
    {
      algorithm: 'HS256',
      expiresIn: "1h"
    }
  );
}
module.exports = createToken;