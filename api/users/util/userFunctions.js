'use strict';
const bcrypt = require('bcrypt');
const Boom = require('boom');
const User = require('../model/User');
const Role = require('../model/Role');

function hashPassword(password, cb) {
  // Generate a salt at level 10 strength
  bcrypt.genSalt(10, (err, salt) => {
    bcrypt.hash(password, salt, (err, hash) => {
      return cb(err, hash);
    });
  });
};

function findRole(_roles) {
  return new Promise((resolve, reject) => {
    getRoles(_roles).then(roles => {
      resolve(roles);
    }).catch(err => reject(err))
  });
};

function getRoles(roles) {
  return new Promise((resolve, reject) => {
    let roleNames = [];
    roles.forEach(role => {
      roleNames.push(role.name);
    });

    Role.find(
      {
        'name': { $in: roleNames }
      }, (err, _roles) => {
        if (_roles === null ||
          _roles.length === 0 || err) {
          reject('Role not found');
          return
        }
        resolve(_roles);
      });
  });
};

function verifyUniqueUser(req, h) {
  return new Promise((resolve, reject) => {
    User.findOne({
      username: req.payload.username
    }, (err, user) => {
      if (user) {
        if (user.username === req.payload.username) {
          reject(Boom.badRequest('Username taken'));
          return
        }
      }
      resolve(req.payload);
    });
  });
};

function verifyCredentials(req, h) {
  const password = req.payload.password;

  return new Promise((resolve, reject) => {
    User.findOne({
      username: req.payload.username
    }, (err, user) => {
      if (user) {
        bcrypt.compare(password, user.password, (err, isValid) => {
          if (isValid) {
            resolve(user);
            return
          }
          else {
            reject(Boom.badRequest('Incorrect password!'));
            return
          }
        });
      } else {
        reject(Boom.badRequest('Incorrect username!'));
      }
    });
  });
};

module.exports = {
  verifyUniqueUser: verifyUniqueUser,
  verifyCredentials: verifyCredentials,
  findRole: findRole,
  hashPassword: hashPassword
};