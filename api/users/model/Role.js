'use strict';
const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const roleModel = new Schema({
    name: { type: String, required: true, unique: true }
});

module.exports = db.model('Role', roleModel);