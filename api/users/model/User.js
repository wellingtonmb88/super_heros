'use strict';
const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const roleModel = new Schema({
    name: { type: String, required: true }
});

const userModel = new Schema({
    username: { type: String, required: true, unique: true },
    password: { type: String, required: true },
    roles: [{ type: roleModel, required: true }],
});

module.exports = db.model('User', userModel);