'use strict';
const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const superPowerModel = new Schema({
    name: { type: String, required: true, unique: true },
    description: { type: String, required: true },
});

module.exports={
    db: db.model('SuperPower', superPowerModel),
    superPowerModel: superPowerModel
}