'use strict';
const Boom = require('boom');
const SuperPower = require('../model/SuperPower').db;
const updateSuperPowerSchema = require('../schemas/updateSuperPower');
const verifyUniqueSuperPower = require('../util/superPowerFunctions').verifyUniqueSuperPower;
const saveAuditEvent = require('../../auditing/util/auditEventFunctions').saveAuditEvent;

function updateSuperPower(req, res) {
  return new Promise((resolve, reject) => {

    SuperPower.findOne({ _id: req.payload._id }, function (err, superPower) {
      if (err || superPower === null) {
        reject(Boom.badRequest("SuperPower undefined!"));
        return
      }

      if (superPower.name !== req.payload.name) {
        verifyUniqueSuperPower(req, res)
          .then(() => { superPower.name = req.payload.name })
          .catch(err => reject(Boom.badRequest(err)))
      }

      superPower.description = req.payload.description;
      superPower.save((err, _superPower) => {
        if (err) {
          reject(Boom.badRequest(err));
          return
        }
        if (_superPower === undefined) {
          reject(Boom.badRequest("SuperPower undefined!"));
          return
        }
        saveAuditEvent("SuperPower", _superPower._id, req.auth.credentials.username, "update");
        resolve(_superPower);
      });
    });
  });
};

module.exports = {
  method: 'PUT',
  path: '/api/superpowers',
  config: {
    handler: async (req, res) => {
      return updateSuperPower(req, res)
        .then(success => { return success })
        .catch(error => { return error })
    },
    validate: {
      payload: updateSuperPowerSchema
    },
    auth: {
      strategy: 'jwt-strategy',
      scope: ['admin']
    }
  }
};