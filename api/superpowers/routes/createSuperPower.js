'use strict';
const Boom = require('boom');
const SuperPower = require('../model/SuperPower').db;
const createSuperPowerSchema = require('../schemas/createSuperPower');
const verifyUniqueSuperPower = require('../util/superPowerFunctions').verifyUniqueSuperPower;
const saveAuditEvent = require('../../auditing/util/auditEventFunctions').saveAuditEvent;

function saveSuperPower(req) {
  return new Promise((resolve, reject) => {
    let superPower = new SuperPower();
    superPower.name = req.payload.name;
    superPower.description = req.payload.description;
    superPower.save((err, superPower) => {
      if (err) {
        reject(Boom.badRequest(err));
        return
      }
      if (superPower === undefined) {
        reject(Boom.badRequest("SuperPower undefined!"));
        return
      }
      saveAuditEvent("SuperPower", superPower._id, req.auth.credentials.username, "create");
      resolve(superPower);
    });
  });
}

module.exports = {
  method: 'POST',
  path: '/api/superpowers',
  config: {
    pre: [
      { method: verifyUniqueSuperPower, assign: 'superpower' }
    ],
    handler: async (req, res) => {
      return saveSuperPower(req)
        .then(success => { return success })
        .catch(error => { return error })
    },
    validate: {
      payload: createSuperPowerSchema
    },
    auth: {
      strategy: 'jwt-strategy',
      scope: ['admin']
    }
  }
}