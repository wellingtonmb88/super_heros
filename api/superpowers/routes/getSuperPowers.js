
'use strict';
const SuperPower = require('../model/SuperPower').db;
const Boom = require('boom');
const Joi = require('joi');

const LIMIT = 100;

function getAllSuperPowers(req) {
  return new Promise((resolve, reject) => {
    const page = req.query.page;
    const offset = (page - 1) * LIMIT;

    SuperPower.find({}, '-__v',
      { skip: offset, limit: LIMIT }, function (err, superPowers) {
        if (err) {
          reject(Boom.badRequest(err));
        }
        resolve({
          page: page,
          count_total: superPowers.length,
          results: superPowers
        });
      });
  });
};

module.exports = {
  method: 'GET',
  path: '/api/superpowers',
  config: {
    validate: {
      query: {
        page: Joi.number().positive()
      }
    },
    handler: async (req, res) => {
      return getAllSuperPowers(req)
        .then(superPowers => { return superPowers })
        .catch(error => { throw error });
    },
    auth: false
  }
};