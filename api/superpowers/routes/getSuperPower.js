
'use strict';
const SuperPower = require('../model/SuperPower').db;
const Boom = require('boom');

function getSuperPower(req) {
  return new Promise((resolve, reject) => {

    SuperPower.findOne({ _id: req.params.id }, '-__v',
      function (err, superpower) {
        if (err) {
          reject(Boom.badRequest(err));
          return
        }
        resolve(superpower);
      });
  });
};

module.exports = {
  method: 'GET',
  path: '/api/superpowers/{id}',
  config: {
    handler: async (req, res) => {
      return getSuperPower(req)
        .then(superpower => { return superpower })
        .catch(error => { throw error });
    },
    auth: false
  }
};