'use strict';
const Boom = require('boom');
const SuperPower = require('../model/SuperPower').db;
const saveAuditEvent = require('../../auditing/util/auditEventFunctions').saveAuditEvent;
const containsSuperPower = require('../../superheros/util/superHeroFunctions').containsSuperPower;

function deleteSuperPower(req) {
  return new Promise((resolve, reject) => {

    containsSuperPower(req.params.id)
      .then(contains => {
        if (contains) {
          reject(Boom.badRequest("SuperPower already in use!"))
        }
      })
      .catch(contains => {
        if (!contains) {
          SuperPower.findOne({ _id: req.params.id }, function (err, superPower) {
            if (err || superPower === null) {
              reject(Boom.badRequest("SuperPower not found!"));
              return
            }
            saveAuditEvent("SuperPower", superPower._id, req.auth.credentials.username, "delete");
            superPower.remove();
            resolve({ success: true, message: "SuperPower deleted successully!" });
          });
        }
      });
  });
};

module.exports = {
  method: 'DELETE',
  path: '/api/superpowers/{id}',
  config: {
    handler: async (req, res) => {
      return deleteSuperPower(req)
        .then(success => { return success })
        .catch(error => { return error });
    },
    auth: {
      strategy: 'jwt-strategy',
      scope: ['admin']
    }
  }
};