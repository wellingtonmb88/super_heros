'use strict';
const Boom = require('boom');
const SuperPower = require('../model/SuperPower').db;

function verifyUniqueSuperPower(req, h) {
    return new Promise((resolve, reject) => {
        SuperPower.findOne({
            name: req.payload.name
        }, (err, superPower) => {
            if (superPower) {
                if (superPower.name === req.payload.name) {
                    reject(Boom.badRequest('SuperPower name taken'));
                    return
                }
            }
            resolve(req.payload);
        });
    });
};

module.exports = {
    verifyUniqueSuperPower: verifyUniqueSuperPower
};