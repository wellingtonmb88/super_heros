'use strict';
const Joi = require('joi');
const updateSuperPowerSchema = Joi.object({
  _id: Joi.string().required(),
  name: Joi.string().min(2).max(30).required(),
  description: Joi.string().required(),
});
module.exports = updateSuperPowerSchema;