'use strict';
const Joi = require('joi');
const createSuperPowerSchema = Joi.object({
  name: Joi.string().min(2).max(30).required(),
  description: Joi.string().required()
});
module.exports = createSuperPowerSchema;